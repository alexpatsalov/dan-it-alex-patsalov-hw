
let tabs = document.getElementsByClassName('services-tab-item');
let tabsContent = document.getElementsByClassName('tab-content');
for (let i = 0; i < tabs.length; i++) {
	tabs[i].onclick = function (event) {
		let tabId = event.target.id - 1;
		for (let j = 0; j < tabs.length; j++) {
			tabs[j].classList.remove('active-tab');
			tabs[tabId].classList.add('active-tab');
			tabsContent[j].classList.remove('tab-content-active');
			tabsContent[tabId].classList.add('tab-content-active');
		}
	}
}

document.getElementById('btnCont').onclick = function () {
	let imagesArray = [
		{src: './img/graphic%20design/graphic-design1.jpg', filter: 'GraphicDesign'},
		{src: './img/graphic%20design/graphic-design2.jpg', filter: 'WebDesign'},
		{src: './img/graphic%20design/graphic-design3.jpg', filter: 'LandingPages'},
		{src: './img/graphic%20design/graphic-design4.jpg', filter: 'Wordpress'},
		{src: './img/graphic%20design/graphic-design5.jpg', filter: 'GraphicDesign'},
		{src: './img/graphic%20design/graphic-design6.jpg', filter: 'WebDesign'},
		{src: './img/graphic%20design/graphic-design7.jpg', filter: 'LandingPages'},
		{src: './img/graphic%20design/graphic-design8.jpg', filter: 'Wordpress'},
		{src: './img/graphic%20design/graphic-design9.jpg', filter: 'GraphicDesign'},
		{src: './img/graphic%20design/graphic-design10.jpg', filter: 'WebDesign'},
		{src: './img/graphic%20design/graphic-design11.jpg', filter: 'LandingPages'},
		{src: './img/graphic%20design/graphic-design12.jpg', filter: 'Wordpress'},
		];
	for (let i = 0; i < 12; i++){
		let createDivFlexItem = document.createElement('div');
		createDivFlexItem.classList.add('flex-item');
		let flexContainer = document.getElementById('flexContainer');
		flexContainer.append(createDivFlexItem);
		let createDivOnHover = document.createElement('div');
		createDivOnHover.classList.add('hidden');
		createDivFlexItem.append(createDivOnHover);
		let createDivIconBox = document.createElement('div');
		createDivIconBox.classList.add('icon-box');
		createDivOnHover.append(createDivIconBox);
		let createIcon = document.createElement('i');
		createIcon.classList.add('fas');
		createIcon.classList.add('fa-stop-circle');
		createDivIconBox.append(createIcon);
		let createParaTop = document.createElement('p');
		createParaTop.innerText = 'Creative Design';
		createParaTop.classList.add('hidden-upper-text');
		createDivOnHover.append(createParaTop);
		let createParaBottom = document.createElement('p');
		createParaBottom.innerText = 'Web Design';
		createParaBottom.classList.add('hidden-bottom-text');
		createDivOnHover.append(createParaBottom);
		let newImg = document.createElement('img');
		newImg.classList.add('flex-img');
		newImg.src = imagesArray[i].src;
		newImg.dataset.filter = imagesArray[i].filter;
		createDivFlexItem.append(newImg);
	}
	let loadButton = document.getElementById('btnCont');
	loadButton.remove();
}

$('.amazing-services-tab-item').on('click', function() {

	$('.amazing-services-tab-item').removeClass('amazing-services-tab-item-active');
	$(this).parent('li').addClass('amazing-services-tab-item-active');
	let image = $(this).attr('data-filter');
	if (image === 'All') {
		$('.flex-img').show();
	} else {
		$('.flex-img').hide();
		$('.flex-img[data-filter="' + image + '"]').show();
	}
});

let currentIndex = 0;
let personCont = document.querySelectorAll('.person-container');
let sliderElement = document.querySelectorAll('.slider-element');
let sliderElementsContainer = document.querySelector('.slider-elements-container');
	sliderElementsContainer.onclick = function (event){
		personCont.forEach((elem, index) => {
			if (event.target.id === elem.dataset.name){
				currentIndex = index;
				sliderElement[index].style.height = '100px';
				sliderElement[index].classList.add('animation');
				personCont[index].classList.add('animation');
				return personCont[index].style.display = 'block';
			}
			sliderElement[index].style.height = 'auto';
			sliderElement[index].classList.remove('animation');
			personCont[index].style.display = 'none';
		});
	}
function notActiveElement() {
	personCont[currentIndex].style.display = 'none';
	personCont[currentIndex].classList.remove('animation');
	sliderElement[currentIndex].style.height = 'auto';
	sliderElement[currentIndex].classList.remove('animation');
}
function activeElement() {
	personCont[currentIndex].style.display = 'block';
	personCont[currentIndex].classList.add('animation');
	sliderElement[currentIndex].style.height = '100px';
	sliderElement[currentIndex].classList.add('animation');
}
function rightClick() {
	notActiveElement();
	currentIndex = (currentIndex + 1)%personCont.length;
	activeElement();
	// timerId = setTimeout(rightClick, 4000);
}

// let timerId2 = setTimeout(rightClick, 0);

let arrowRight = document.getElementById('arrowRight');
	arrowRight.onclick = function () {
	rightClick();
};

function leftClick() {
	notActiveElement();
	if(currentIndex === 0){
		currentIndex = personCont.length;
	}
	currentIndex = (currentIndex - 1)%personCont.length;
	activeElement();
}
let arrowLeft = document.getElementById('arrowLeft');
arrowLeft.onclick = function () {
	leftClick();
};

