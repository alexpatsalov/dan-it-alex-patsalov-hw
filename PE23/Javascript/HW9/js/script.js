// Реализовать переключение вкладок (табы) на чистом Javascript.
// 	Технические требования:
//// 	В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой вкладки.
// 	Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// 	Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться. При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.

let tabs = document.getElementsByClassName('tabs-title');

let tabsContent = document.getElementsByClassName('tab');

for (let i = 0; i < tabs.length; i++) {
	tabs[i].onclick = function (event) {
		let tabId = event.target.id - 1;
		for (let j = 0; j < tabs.length; j++) {
			tabs[j].classList.remove('active');
			tabs[tabId].classList.add('active');
			tabsContent[j].classList.remove('active');
			tabsContent[tabId].classList.add('active');
		}
	}
}