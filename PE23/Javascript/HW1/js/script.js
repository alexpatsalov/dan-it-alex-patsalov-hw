let name;
let age;
let ageNumber;

do {
    name = prompt ('What is your name?', name);
} while (name === '' || !isNaN(name));

do {
    age = prompt ('How old are you?', age);
    ageNumber = +age;
} while (age === '' || isNaN(ageNumber));

if (ageNumber < 18) {
    alert('You are not allowed to visit this website!');
} else if (ageNumber > 22) {
    alert('Welcome, ' + name);
} else {
    if (ageNumber <= 22 || ageNumber >= 18) {
        userAnswer = confirm('Are you sure you want to continue?');
    } if (userAnswer) {
        alert('Welcome, ' + name);
    } else {
        alert('You are not allowed to visit this website!');
    }
}