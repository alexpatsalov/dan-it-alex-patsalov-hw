
let images = document.getElementsByTagName('img');
let index = 1;
let timerId2;

function slider() {
	if (index < images.length) {
		for (let element of images) {
			element.classList.remove('active');
			element.classList.add('hidden');
		}
		images[index].classList.add('active');
		images[index].classList.remove('hidden');
		index++;
	} else {
		index = 0;
	}
	console.log(Date.now());
	timerId2 = setTimeout(slider, 10000);
}

let timerId = setTimeout(slider, 0);

document.getElementById('buttonStop').onclick = function (e) {
	clearTimeout(timerId);
	clearTimeout(timerId2);
	console.log('Slider stopped!');
};

document.getElementById('buttonResume').onclick = function (e) {
	setTimeout(slider, 0);
	console.log('Slider resumed!');
}