
let htmlWithBody = $('html, body');
$(document).ready(function (){
	console.log('DOM ready!');
		$('.anchor-link').click(function (e) {
			e.preventDefault();
			let anchorLink = $(this).attr('href');
			let destination = $(anchorLink).offset().top;
			$('html, body').animate({
				scrollTop: destination,
			}, 1000);
		});
		return false;

});

let buttonUp = $('#up-button');
let screenHeight = $(window).height();
$(window).scroll(function () {
	if ($(window).scrollTop() > screenHeight){
		buttonUp.addClass('active');
	} else {
		buttonUp.removeClass('active');
	}
});
buttonUp.click(function (e) {
	e.preventDefault();
	htmlWithBody.animate({scrollTop: 0}, 300);
});

let buttonToggle = $('.toggle-button');
buttonToggle.click(function(){
	$('#most-popular-posts').slideToggle();
});