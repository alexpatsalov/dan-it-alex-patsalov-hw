function mathOperation(number1, number2, operation) {
    switch (operation) {
        case '+':
            res = number1 + number2;
            return res;
        case '-':
            return number1 - number2;
        case '*':
            return number1 * number2;
        case '/':
            return number1 / number2;
    }
}

let number1;
let number2;
let res;

do{
    number1 = +prompt('Insert number 1:', number1);
} while (number1 === 0 || Number.isNaN(number1));

do{
    number2 = +prompt('Insert number 2:', number2);
} while (number2 === 0 || Number.isNaN(number2));

let operation = prompt('Insert operation (+, -, *, /):');

console.log('result --->> ', mathOperation(number1, number2, operation));
