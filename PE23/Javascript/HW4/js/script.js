// Технические требования:
//
//     Написать функцию createNewUser(), которая будет создавать и возвращать объект newUser.
//     При вызове функция должна спросить у вызывающего имя и фамилию.
//     Используя данные, введенные пользователем, создать объект newUser со свойствами firstName и lastName.
//     Добавить в объект newUser метод getLogin(), который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя, все в нижнем регистре (например, Ivan Kravchenko → ikravchenko).
// Создать пользователя с помощью функции createNewUser(). Вызвать у пользователя функцию getLogin(). Вывести в консоль результат выполнения функции.
//
//     Необязательное задание продвинутой сложности:
//
//     Сделать так, чтобы свойства firstName и lastName нельзя было изменять напрямую. Создать функции-сеттеры setFirstName() и setLastName(), которые позволят изменить данные свойства.

function createNewUser(firstNameIn, lastNameIn){

    return {
        "firstName" : firstNameIn,
        "lastName" : lastNameIn,
        getLogin : function () {
                    return ((this.firstName[0] + this.lastName).toLowerCase())
        }
    }
}

let firstName = prompt ('Insert first name:');
let lastName = prompt ('Insert last name:');

console.log(createNewUser(firstName, lastName).getLogin());

