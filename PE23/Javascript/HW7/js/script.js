// 	Создать функцию, которая будет принимать на вход массив.
////// 	Каждый из элементов массива вывести на страницу в виде пункта списка
//// Необходимо использовать шаблонные строки и функцию map массива для формирования контента списка перед выведением его на страницу.
//// 	Примеры массивов, которые можно выводить на экран:
// 	['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv']
// 		['1', '2', '3', 'sea', 'user', 23]
////// Можно взять любой другой массив.

function showList(array){
	let createUl = document.createElement('ul');
	let list = array.map((item) => {
		return `<li>${item}</li>`;
	});
	console.log(list);
	document.body.append(createUl);
	createUl.insertAdjacentHTML("afterbegin", list.join(''));
}
let arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
showList(arr);







// function showList(array){
// 	let createUl = document.createElement('ul');
// 	let list = array.map((item) => {
// 		return `${item}`;
// 	});
// 	document.body.append(createUl);
// 	for (let i = 0; i < list.length; i++){
// 		let createLi = document.createElement('li');
// 		createUl.append(createLi);
// 		createLi.innerHTML = list[i];
// 	}
// }
// let arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv'];
// showList(arr);
