// При вызове функция должна спросить у вызывающего дату рождения (текст в формате dd.mm.yyyy) и сохранить ее в поле birthday.
// 	Создать метод getAge() который будет возвращать сколько пользователю лет.
// 	Создать метод getPassword(), который будет возвращать первую букву имени пользователя в верхнем регистре, соединенную с фамилией (в нижнем регистре) и годом рождения. (например, Ivan Kravchenko 13.03.1992 → Ikravchenko1992).
//
// Вывести в консоль результат работы функции createNewUser(), а также функций getAge() и getPassword() созданного объекта.

function createNewUser(firstNameIn, lastNameIn){

	return  {
		"firstName" : firstNameIn,
		"lastName" : lastNameIn,
		getLogin : function () {
			return `${this.firstName[0].toLowerCase()}${this.lastName.toLowerCase()}`
		},
		getAge : function (){
			let year = Number(birthdayStr.substr(6,4));
			let month = Number(birthdayStr.substr(3,2));
			let day = Number(birthdayStr.substr(0,2));
			let birthday = new Date (year, month, day);
			let today = new Date ();
			return today.getFullYear() - birthday.getFullYear();
		},
		getPassword : function () {
			let year = Number(birthdayStr.substr(6,4));
			return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${year}`
		}
// Вариант 2, через массив.
		// 		getAge : function (){
// 			const birthdayArr = birthdayStr.split('.');
// 			let birthdayInMls = new Date (Number(birthdayArr[2]), Number(birthdayArr[1]-1), Number(birthdayArr[0]));
// 			let today = new Date ();
// 			return today.getFullYear() - birthdayInMls.getFullYear();
// 		},
// 		getPassword : function () {
// 			const birthdayArr = birthdayStr.split('.');
// 			return `${this.firstName[0].toUpperCase()}${this.lastName.toLowerCase()}${birthdayArr[2]}`
// 		}
	}
}

let firstName = prompt ('Insert first name:');
let lastName = prompt ('Insert last name:');
let birthdayStr = prompt ('Insert your Birthday: DD.MM.YYYY');

console.log('Function is working---->>>', createNewUser(firstName, lastName));
console.log('Your age is ----------->>>', createNewUser(firstName, lastName).getAge());
console.log('Your password is ------>>>', createNewUser(firstName, lastName).getPassword());

