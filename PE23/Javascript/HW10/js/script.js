document.addEventListener('click', function (event) {

	let target = event.target;

	if (target.dataset.eye !== undefined) {
		target.classList.toggle('fa-eye');
		target.classList.toggle('fa-eye-slash');

		if (target.previousElementSibling.getAttribute('type') === 'password'){
			target.previousElementSibling.setAttribute('type', 'text');
			} else {
			target.previousElementSibling.setAttribute('type', 'password');
			}
	}
});

document.getElementById('submitBtn').onclick = function () {
	if (document.getElementById('input').value === document.getElementById('inputCheck').value) {
		alert('You are welcome!');
	} else {
		let errorSpan = document.createElement('span');
		errorSpan.id = "error";
		if (document.getElementById('error') === null) {
			errorSpan.innerHTML = 'Нужно ввести одинаковые значения!!';
			errorSpan.setAttribute('class', 'red-font');
			document.getElementById('submitBtn').before(errorSpan);
		}
	}
	if (document.getElementById('error') !== null && document.getElementById('input').value === document.getElementById('inputCheck').value){
		document.getElementById('error').remove();
	}
}