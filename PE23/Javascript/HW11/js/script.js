
let buttons = document.getElementsByTagName('button');

document.addEventListener('keydown', function(event) {

	for (const elem of buttons){
		if (elem.classList.contains('blue-color-back')) {
			elem.classList.remove('blue-color-back');
		}
		if (elem.id === event.code){
			elem.classList.add('blue-color-back');
		}
	}
});