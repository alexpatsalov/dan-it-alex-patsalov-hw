function onInputFocus(){
	document.getElementById('input-number').classList.add('green-frame');
	}
function outInputFocus(){
	document.getElementById('input-number').classList.remove('green-frame');
}

function submit() {
	let inputValue = document.querySelector('input').value;

	if (inputValue >= 0){
		if (document.getElementById('spanElement') !== null) {
			document.getElementById('spanElement').innerHTML = `Текущая цена ${inputValue}<button type="reset" class = "reset-button" id = "clearButton" onclick="">X</button>`;
		} else {
			document.body.insertAdjacentHTML("afterbegin", `<span id="spanElement" class="current-value-true">Текущая цена ${inputValue}<button type="reset" class = "reset-button" id = "clearButton" onclick="">X</button></span>`);
		}
		document.getElementById("clearButton").onclick = function() {
			document.getElementById("input-number").value = "";
			document.getElementById('spanElement').remove();
			if (document.getElementById('error-span') !== null){
				document.getElementById('error-span').remove();
			}
		}
	} else {
		if (document.getElementById('error-span') === null) {
			document.body.insertAdjacentHTML("beforeend", `<span id="error-span" class="current-value-error">Please enter correct price</span>`);
		}
		else {
			document.getElementById('error-span').classList.add('red-font');
			document.getElementById('input-number').classList.add('red-frame');
		}
	}
	document.getElementById('input-number').classList.add('green-font');
}



