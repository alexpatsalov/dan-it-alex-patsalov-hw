function initialTheme (themeName){
	localStorage.setItem('theme', themeName);
	document.getElementById('table').className = themeName;
}

function changeTheme() {
	if(localStorage.getItem('theme') === 'dark'){
		initialTheme('light');
	} else {
		initialTheme('dark');
	}
}
document.getElementById('btnTheme').addEventListener('click', function () {
	changeTheme();
});

document.addEventListener('DOMContentLoaded', function (){
	initialTheme(localStorage.getItem('theme'));
});