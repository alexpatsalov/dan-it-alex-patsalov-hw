
// Технические требования:
//
// 	Написать функцию filterBy(), которая будет принимать в себя 2 аргумента. Первый аргумент - массив, который будет содержать в себе любые данные, второй аргумент - тип данных.
// 	Функция должна вернуть новый массив, который будет содержать в себе все данные, которые были переданы в аргумент, за исключением тех, тип которых был передан вторым аргументом. То есть, если передать массив ['hello', 'world', 23, '23', null], и вторым аргументом передать 'string', то функция вернет массив [23, null].

// Вариант 1 - самый оптимальный
function filterBy(arr, type){
	return arr.filter(function (item) {
		return typeof item !== type;
	})
}

let typeOfData = prompt ('Insert any type of data');
let myArray = [12412, 'green', '14214124', null, true, false, undefined, 23423, '325', [], {}];

console.log('Result ---->>>', filterBy(myArray, typeOfData));

// Вариант 2
// function filterBy(arr, type){
// 	for (let i = 0; i <= arr.length; i++ ){
// 		if (typeof(arr[i]) !== type){
// 			filteredArray.push(arr[i]);
// 		}
// 	}
// }


// Вариант 3
// function filterBy(arr, type){
// 	arr.forEach(function(item, i, arr){
// 		if (typeof(arr[i]) !== type){
// 			filteredArray.push(arr[i]);
// 		}
// 	})
// }
// let typeOfData = prompt ('Insert any type of data');
// let myArray = [12412, 'green', '14214124', null, true, false, undefined, 23423, '325', [], {}];
// let filteredArray = [];
//
// filterBy(myArray, typeOfData);
// console.log(filteredArray);