let btn = document.getElementById('btn');
let ipFromUrl = 'https://api.ipify.org/?format=json';
let ipPhysicalInfo = 'http://ip-api.com/json/';

btn.onclick = async function () {
    let {ip} = await getIpInfo();
    // console.log(ip);
    let data = await processUrl(`${ipPhysicalInfo}${ip}`);
    // console.log(data);
    let ulTag = document.createElement('ul');
    document.body.appendChild(ulTag);
    // console.log(data);
    for (let key in data) {
        let liTag = document.createElement('li');
        liTag.innerHTML = `<strong>${key}</strong> : ${data[key]}`;
        ulTag.appendChild(liTag);
        // console.log(`${key} -->> ${data[key]}`);
    }
}

async function processUrl(url) {
    const response = await fetch(url);
    // console.log(response);
    return response.json();
}

async function getIpInfo() {
    return processUrl(ipFromUrl);
}

// btn.onclick = function () {
// 	getIpInfo()
// 		.then(({ip}) => {
// 			// console.log(ip);
// 			return processUrl(`${ipPhysicalInfo}${ip}`)
// 		})
// 		.then((data) =>{
// 			let ulTag = document.createElement('ul');
// 			document.body.appendChild(ulTag);
// 			// console.log(data);
// 			for (let key in data){
// 				let liTag = document.createElement('li');
// 				liTag.innerHTML = `<strong>${key}</strong> : ${data[key]}`;
// 				ulTag.appendChild(liTag);
// 				// console.log(`${key} -->> ${data[key]}`);
// 			}
// 		})
//
// }


