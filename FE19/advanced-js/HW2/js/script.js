const books = [
	{
		author: "Скотт Бэккер",
		name: "Тьма, что приходит прежде",
		price: 70
	},
	{
		author: "Скотт Бэккер",
		name: "Воин-пророк",
	},
	{
		name: "Тысячекратная мысль",
		price: 70
	},
	{
		author: "Скотт Бэккер",
		name: "Нечестивый Консульт",
		price: 70
	},
	{
		author: "Дарья Донцова",
		name: "Детектив на диете",
		price: 40
	},
	{
		author: "Дарья Донцова",
		name: "Дед Снегур и Морозочка",
	}
];

let root = document.getElementById('root');
let ulTag = document.createElement('ul');
root.append(ulTag);

for (let i = 0; i < books.length; i++) {

	try {

		let valid = Array.from(`<li>${Object.values(new Book(books[i])).toString()}</li>`);
		ulTag.insertAdjacentHTML("afterbegin", valid.join(''))

	} catch (e) {

		console.log(e);

	}
}

function Book({author, name, price}) {

	if (!author) {
		throw new Error(`Author is required`);
	}
	if (!name) {
		throw new Error(`Name is required`);
	}
	if (!price) {
		throw new Error(`Price is required`);
	}

	this.author = author;
	this.name = name;
	this.price = price;
}


