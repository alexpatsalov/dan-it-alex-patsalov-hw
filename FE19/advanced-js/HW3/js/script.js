
class Employee {
	constructor(name = 'Ivan', age = 30, salary = 10000) {
		this.name = name;
		this.age = age;
		this.salary = salary;
	};
	set name(value) {
		this._name = value;
	}
	get name() {
		return this._name;
	}
	set setAge(age) {
		this.age = age;
	}
	get getAge() {
		return this.age;
	}
	set setSalary(salary) {
		this.salary = salary;
	}
	get getSalary() {
		return this.salary;
	}
}

class Programmer extends Employee {
	constructor(name, age, salary, lang = 'en') {
		super(name, age, salary, lang);
		this.lang = lang;
	}
	get getSalary() {
		return this.salary * 3;
	}
}

let person = new Programmer('Max', 15, 100, 'ru');
let human = new Programmer('Olena', 20, 500);
let dog = new Programmer('Sharik', 2, 20);
console.log(person);
console.log(dog);
console.log(human);
