let ulTag = document.createElement('ul');
document.body.appendChild(ulTag);

document.getElementById('btn').onclick = function () {
	getFilms('https://swapi.dev/api/films/')
}

function getFilms(url) {
	fetch(url)
		.then(function (response) {
			return response.json();
		})
		.then(function ({results}) {
			// console.log(results); //Array of Films-Objects
			return results
				.forEach(function ({title, episode_id, opening_crawl, characters}) {
					let filmUlTag = document.createElement('ul');
					filmUlTag.innerHTML = `${title}, episode No ${episode_id}`;
					ulTag.appendChild(filmUlTag);
					let openingCrawlLi = document.createElement('li');
					openingCrawlLi.innerHTML = opening_crawl;
					filmUlTag.appendChild(openingCrawlLi);
					// console.log(characters); //Array of urls of characters
					Promise
						.all(characters.map(function (item) {
							return fetch(item)
								.then(function (response) {
									return response.json();
								})
								.then(function ({name}) {
									// console.log(name); //Names picked up from all Objects of characters
									let liTagForNames = document.createElement('li');
									liTagForNames.innerHTML = name;
									filmUlTag.appendChild(liTagForNames);
								})
						}))
				})
		});
}





