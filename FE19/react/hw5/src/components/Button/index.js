import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss'

export default function Button ({onClick, toCart, modification, text}) {

    const onBtnClick = (e) => {
        onClick(e);
    }

        return (
            <button data-cart={toCart} className={'button button-' + modification} onClick={onBtnClick}>{text}</button>
        );
}

Button.propTypes = {
    onBtnClick: PropTypes.func,
    modification: PropTypes.string,
    text: PropTypes.string
}