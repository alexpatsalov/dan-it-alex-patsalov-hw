export const ADD_TO_FAVOURITES = 'ADD_TO_FAVOURITES';
export const DELETE_FROM_FAVOURITES = 'DELETE_FROM_FAVOURITES';

export const favouritesAddAction = (item) => (dispatch) => {

    dispatch({
        type: ADD_TO_FAVOURITES,
        payload: item
    })
}

export const favouritesDeleteAction = (item) => (dispatch) => {

    dispatch({
        type: DELETE_FROM_FAVOURITES,
        payload: item
    })
}
