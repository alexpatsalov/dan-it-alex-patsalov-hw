import {ADD_TO_CART, DELETE_FROM_CART} from "../actions/cartAction";

const initialState = {
    cart: []
}

export function cartReducer(state = initialState, action) {

    switch (action.type) {

        case ADD_TO_CART:

            return {
                ...state,
                cart: [...state.cart, action.payload]
            }

        case DELETE_FROM_CART:

            return {
                cart: [...state.cart.filter(item => item.id !== action.payload.id)]
            }

        default:
            return state
    }
}