import axios from 'axios';

export default class ProductService{
    static getProductList(){
        return axios.get('http://localhost:3000/productList.json');
    }
}