import React from 'react';
import './App.css'
import ProductService from "./services/productService";
import ProductList from "./components/ProductList";
import Header from "./components/Header";

export default class App extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: [],
            favourites: [],
            cart: []
        }
        this.addToFavourites = this.addToFavourites.bind(this);
        this.addToCart = this.addToCart.bind(this);
    }

    addToCart(e) {
        let {cart} = this.state;
        let clonedCart = [...cart];
        let product = JSON.parse(e.target.dataset.cart);
        clonedCart.push(product);
        this.setState(() => ({
            cart: [...clonedCart]
        }));
        localStorage.setItem('cart', JSON.stringify(clonedCart));
    }

    addToFavourites(e) {
        const {favourites} = this.state;
        let clickedStar = e.target;
        let clickedProduct = JSON.parse(e.target.dataset.product);
        let clonedFavourites = [...favourites];

        if (!clickedStar.className.includes('button-star-clicked') && !favourites.includes(clickedProduct)) {
            clickedStar.className += ' button-star-clicked';
            clonedFavourites.push(clickedProduct);
            localStorage.setItem('favourites', JSON.stringify(clonedFavourites));
            this.setState(() => ({
                favourites: [...clonedFavourites]
            }))

        } else if (clickedStar.className.includes('button-star-clicked')) {
            clickedStar.classList.remove('button-star-clicked');
            const newFavourites = favourites.filter(function (item) {
                return item.id !== clickedProduct.id
            })
            localStorage.setItem('favourites', JSON.stringify(newFavourites));
            this.setState(() => ({
                favourites: newFavourites
            }))
        }
    }

    async componentDidMount() {
        const {data} = await ProductService.getProductList();
        this.setState({
            products: data
        });
    }

    render() {
        console.log('RENDER');

        return (
            <div>
                <Header favourites={this.state.favourites.length} cart={this.state.cart.length}/>
                <ProductList items={this.state.products} onClick={this.addToFavourites} onSuccess={this.addToCart}/>
            </div>
        );
    }
}
