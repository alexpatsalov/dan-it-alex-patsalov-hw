import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss'

export default class Button extends React.Component {
    constructor(props) {
        super(props);

        this.onBtnClick = this.onBtnClick.bind(this);
    }

    onBtnClick(e) {
        this.props.onClick(e);
    }

    render() {

        return (
            <button data-cart={this.props.toCart} className={'button button-' + this.props.modification} onClick={this.onBtnClick}>{this.props.text}</button>
        );

    }
}

Button.propTypes = {
    onBtnClick: PropTypes.func,
    modification: PropTypes.string,
    item: PropTypes.object
}