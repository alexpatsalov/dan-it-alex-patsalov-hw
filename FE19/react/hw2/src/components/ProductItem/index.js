import React from 'react';
import '../ProductList/ProductList.scss'
import PropTypes from "prop-types";

export default class ProductItem extends React.Component {

    render() {
        return (
            <>
                <h4>{this.props.item.name}
                    <button className='button button-star' data-product={JSON.stringify(this.props.item)}
                            onClick={this.props.onClick}>&#9733;</button>
                </h4>
                <p>{this.props.item.price}</p>
                <div className='image-container'>
                    <img alt='broken' className='product-image' src={this.props.item.url}/>
                </div>
                <p>Color: {this.props.item.color}</p>
                <p>Item No: {this.props.item.article}</p>
            </>
        );
    }
}
ProductItem.propTypes = {
    item: PropTypes.object,
    onClick: PropTypes.func
}