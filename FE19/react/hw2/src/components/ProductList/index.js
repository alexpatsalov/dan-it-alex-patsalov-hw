import React from 'react';
import PropTypes from 'prop-types';
import './ProductList.scss';
import '../Button/Button.scss'
import Button from "../Button";
import Modal from "../Modal";
import ProductItem from "../ProductItem";

export default class ProductList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false,
            product: null
        }

        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
        this.onSuccess = this.onSuccess.bind(this);
    }

    openModal(e) {
        this.setState({
            show: true,
            product: e.target.dataset.cart
        })
    }

    closeModal() {
        this.setState({
            show: false
        })
    }

    onSuccess(e) {
        this.props.onSuccess(e)
    }

    render() {

        return (
            <div className='container'>
                <Modal show={this.state.show}
                       product={this.state.product}
                       onSuccess={this.onSuccess}
                       onClick={this.closeModal}
                       onClose={this.closeModal}
                />
                {
                    this.props.items.map
                    (item =>
                        <div className='product-container'
                             key={item.id}>

                            <Button modification='add-to-cart'
                                    onClick={this.openModal}
                                    text='Add to cart'
                                    toCart={JSON.stringify(item)}/>


                            <ProductItem item={item}
                                         onClick={this.props.onClick}
                            />
                        </div>
                    )
                }
            </div>
        );
    }
}

ProductList.propTypes = {
    items: PropTypes.array,
    onClick: PropTypes.func
}