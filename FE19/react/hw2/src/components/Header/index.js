import React from 'react';
import './Header.scss';
import PropTypes from "prop-types";

export default class Header extends React.Component{

    render(){
        return(
          <div className='header-container'>
              <div className='header-cart'>{this.props.cart} items in cart</div>
              <div className='header-favourites'>{this.props.favourites} items in favourites</div>
          </div>
        );
    }
}

Header.propTypes = {
    cart: PropTypes.number,
    favourites: PropTypes.number
}