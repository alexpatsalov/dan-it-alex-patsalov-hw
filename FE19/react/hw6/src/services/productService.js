const axios = require('axios');

class Ajax {
    static async getData() {
        return await axios.get('http://localhost:3000/productList.json');
    }
}

module.exports = Ajax;