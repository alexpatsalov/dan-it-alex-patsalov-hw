import React from 'react';
import ProductItem from "./index";

const props = {
    item:{name: 'name', price: 10, color: 'red', article: 124},
    onClick: () => {},
    showStar:true,
    showCross:false,
    isActive:true
}

const setUp = (props) => shallow (<ProductItem {...props} />);

describe('ProductItem component', ()=>{
    let component = setUp(props);

    test('should render ProductItem component', ()=>{
        expect(component).toMatchSnapshot();
    })
    test('should render heading', ()=>{
        let heading = component.find("h4");
        expect(heading).toHaveLength(1);
    })
})