import React from 'react';
import {Switch, Route} from 'react-router-dom';
import Home from "../Home";
import Favourites from "../Favourites";
import Cart from "../Cart";

export default function Main() {

    return (
        <main>
            <Switch>
                <Route exact path='/' render={() => <Home/>}/>
                <Route path='/favourites' render={() => <Favourites/>}/>
                <Route path='/cart' render={() => <Cart/>}/>
            </Switch>

        </main>
    );
}