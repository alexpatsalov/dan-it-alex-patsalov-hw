import React from 'react';
import Button from "./index";

describe('Button component', ()=>{

    test('should render button component', ()=>{
        let component = shallow (<Button onClick={[Function]} toCart='toCart' modification='modification' text='Test text'/>);
        expect(component).toMatchSnapshot();
    })

    test('click testing', ()=>{
        const onBtnClick = (e) =>{
            return e
        }
        let component = shallow (<Button onClick={onBtnClick} toCart='toCart' modification='modification' text='Test text'/>);
        expect(component).toMatchSnapshot();
    })

})
