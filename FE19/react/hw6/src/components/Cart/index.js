import React, {useState} from 'react';
import Modal from "../Modal";
import ProductItem from "../ProductItem";
import {useDispatch, useSelector} from "react-redux";
import {cartDeleteAction} from "../../redux/actions/cartAction";
import {modalHideAction, modalShowAction} from "../../redux/actions/modalAction";
import FormForPurchase from "../Form";

export default function Cart() {

    const [product, setProduct] = useState({});
    const [showPurchaseWindow, setShowPurchaseWindow] = useState(false);
    const [cart, setCart] = useState([]);
    const cartInLS = JSON.parse(localStorage.getItem('cart'));
    const showModal = useSelector(state => state.modalReducer.show);
    const dispatch = useDispatch();

    const deleteItem = (e) => {
        let product = JSON.parse(e.target.dataset.cart);
        setCart([product]);
        const filtered = cartInLS.filter((item) => item.id !== product.id);;
        dispatch(cartDeleteAction(product));
        setCart(filtered);
        localStorage.setItem('cart', JSON.stringify(filtered));
    }


    const openModal = (e) => {
        dispatch(modalShowAction())
        setProduct(e.target.dataset.product);
    }

    const closeModal = () => {
        dispatch(modalHideAction())
    }

    const openPurchaseWindow = () => {
        setShowPurchaseWindow(true)
    }

    const closePurchaseWindow = () => {
        setShowPurchaseWindow(false)
    }

    return (
        <div>
            <h3>This is Cart Page</h3>
            <h4>There are {cartInLS.length} items in cart</h4>
            <div className="button-container">
                <button style={{border: '1px solid black',
                    fontSize: '20px',
                    padding: '10px',
                    borderRadius: '30%',
                    backgroundColor: 'chartreuse',
                    cursor: 'pointer'}}
                        type='button' onClick={openPurchaseWindow}>Open form </button>
            </div>
            <FormForPurchase cart={cart} setCart={setCart} show={showPurchaseWindow} onClose={closePurchaseWindow}/>
            <div className='container'>
                <Modal show={showModal}
                       product={product}
                       onSuccessAdd={deleteItem}
                       onClick={closeModal}
                       onClose={closeModal}
                       textBody={'Delete?'}
                       textHeader={'Are you sure you want to delete this?'}
                />
                {
                    cartInLS.map
                    (item =>
                        <div className='product-container'
                             key={JSON.stringify(item.id)}>

                            <ProductItem item={item} showArrow={false} showCross={true}
                                         onClick={openModal}/>
                        </div>
                    )
                }
            </div>
        </div>
    );
}