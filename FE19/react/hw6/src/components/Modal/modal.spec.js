import React from 'react';
import Modal from "./index";
import { Provider } from 'react-redux'

const ReduxProvider = ({ children, reduxStore }) => (
    <Provider store={reduxStore}>{children}</Provider>
)

const props = {
    product: 'product test',
    onClose: () => {},
    onSuccessAdd: ()=>{},
    textBody: 'text body test',
    textHeader: 'test header test'
}

const setUp = (props) => shallow (<ReduxProvider><Modal {...props} /></ReduxProvider>);

describe('Modal component', ()=>{
    let component = setUp(props);

    test('should render Modal component', ()=>{
        expect(component).toMatchSnapshot();
    })

})