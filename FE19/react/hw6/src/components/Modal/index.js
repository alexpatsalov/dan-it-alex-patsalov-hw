import React from 'react';
import PropTypes from 'prop-types';
import Button from "../Button";
import './Modal.scss';
import {useSelector} from "react-redux";


export default function Modal({product, onClose, onSuccessAdd, textBody, textHeader}) {

    const showModal = useSelector(state => state.modalReducer.show);

    const onCloseArea = (e) => {
        if (e.target.className === 'modal') {
            onClose()
        }
    }
    const onSuccess = (e) => {
        onSuccessAdd(e);
        onClose();
    }

    let displayModal = {
        display: showModal ? 'flex' : 'none'
    }

    return (
        <div className='modal' style={displayModal} onClick={onCloseArea}>
            <div className='modal-content'>
                <div className='modal-header'>
                    <h4>{textHeader}</h4>
                    <div className='button-container'>
                        <Button modification='arrow' onClick={onClose} text='&times;'/>
                    </div>
                </div>
                <div className='modal-body'>
                    <p>{textBody}</p>
                </div>
                <div className='modal-footer'>
                    <Button modification='action' toCart={product} onClick={onSuccess} text='Ok'/>
                    <Button modification='action' onClick={onClose} text='Cancel'/>
                </div>
            </div>
        </div>
    );
}

Modal.propTypes = {
    onClose: PropTypes.func,
    onSuccess: PropTypes.func,
    show: PropTypes.bool
}
