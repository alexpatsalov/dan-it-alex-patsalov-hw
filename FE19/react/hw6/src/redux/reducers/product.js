import {LOAD_PRODUCTS_ERROR, LOAD_PRODUCTS_PROCESS, LOAD_PRODUCTS_SUCCESS} from "../actions/productListAction";

const initialState = {
    products: [],
    isLoading: true,
    errored: true
}

export function productReducer(state = initialState, action){

    switch (action.type){

        case LOAD_PRODUCTS_SUCCESS:
            return {
                ...state,
                products: action.payload
            }

        case LOAD_PRODUCTS_PROCESS:
            return {
                ...state,
                isLoading: action.payload
            }

        case LOAD_PRODUCTS_ERROR:
            return {
                ...state,
                errored: action.payload
            }

        default:
            return state
    }
}