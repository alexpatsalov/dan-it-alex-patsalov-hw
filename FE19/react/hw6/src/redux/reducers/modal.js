import {SHOW_MODAL, HIDE_MODAL} from "../actions/modalAction";

const initialState = {
    show: false
}

export function modalReducer(state = initialState, action){

    switch (action.type){

        case SHOW_MODAL:
            return {
                show: action.payload
            }

        case HIDE_MODAL:
            return {
                show: action.payload
            }

        default:
            return state
    }
}