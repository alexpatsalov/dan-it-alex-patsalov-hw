import {ADD_TO_FAVOURITES, DELETE_FROM_FAVOURITES} from "../actions/favouritesAction";

const initialState = {
    favourites: []
}

export function favouritesReducer(state = initialState, action){

    switch (action.type){

        case ADD_TO_FAVOURITES:

            return {
                ...state,
                favourites: [...state.favourites, action.payload]
            }

        case DELETE_FROM_FAVOURITES:

            return {
                favourites: [...state.favourites.filter(item => item.id !== action.payload.id)]
            }

        default:
            return state
    }
}