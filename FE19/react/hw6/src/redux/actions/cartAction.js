export const ADD_TO_CART = 'ADD_TO_CART';
export const DELETE_FROM_CART = 'DELETE_FROM_CART';
export const CART_SUBMIT = 'CART_SUBMIT';

export const cartAddAction = (item) => (dispatch) => {

    dispatch({
        type: ADD_TO_CART,
        payload: item
    })
}

export const cartDeleteAction = (item) => (dispatch) => {

    dispatch({
        type: DELETE_FROM_CART,
        payload: item
    })
}

export const cartSubmit = () => (dispatch) =>{

    let cart = localStorage.getItem('cart');
    console.log('items in cart -->', JSON.parse(cart));
    localStorage.setItem('cart', '[]');

    dispatch({
        type: CART_SUBMIT,
        payload: []
    })
}