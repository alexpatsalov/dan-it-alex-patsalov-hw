import Ajax from '../../services/productService'

export const LOAD_PRODUCTS_SUCCESS = 'LOAD_PRODUCTS_SUCCESS';
export const LOAD_PRODUCTS_PROCESS = 'LOAD_PRODUCTS_PROCESS';
export const LOAD_PRODUCTS_ERROR = 'LOAD_PRODUCTS_ERROR';

export const productListAction = () => async (dispatch) => {
    const {data} = await Ajax.getData();

    dispatch({
        type: LOAD_PRODUCTS_SUCCESS,
        payload: data
    })

}

export const productListLoading = (bool) => (dispatch) => {
    dispatch({
        type: LOAD_PRODUCTS_PROCESS,
        payload: bool
    });
}

export const productListError = (bool) => (dispatch) => {
    dispatch({
        type: LOAD_PRODUCTS_ERROR,
        payload: bool
    });
}