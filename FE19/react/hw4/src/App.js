import React from 'react';
import './App.css'
import Main from "./components/Main";
import NavigationLinks from "./components/NavigationLinks";

export function App() {

    console.log('---RENDER---');

    return (
        <div className="App">
            <NavigationLinks/>
            <Main/>
        </div>
    );
}
