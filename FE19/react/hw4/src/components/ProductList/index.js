import React from 'react';
import PropTypes from 'prop-types';
import './ProductList.scss';
import '../Button/Button.scss'
import Button from "../Button";
import Modal from "../Modal";
import ProductItem from "../ProductItem";
import {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {modalHideAction, modalShowAction} from "../../redux/actions/modalAction";

export default function ProductList(props) {

    const [product, setProduct] = useState(null);
    const showModal = useSelector(state => state.modalReducer.show);
    const favouritesArray = useSelector(state => state.favouritesReducer.favourites);
    const dispatch = useDispatch();

    const openModal = (e) =>{
        dispatch(modalShowAction())
        setProduct(e.target.dataset.cart);
    }

    const closeModal = () => {
        dispatch(modalHideAction());
    }

    return (
        <div className='container'>
            <Modal show={showModal}
                   product={product}
                   onSuccessAdd={props.onSuccess}
                   onClick={closeModal}
                   onClose={closeModal}
                   textHeader={'Are you sure you want to buy this?'}
                   textBody={'Add to cart'}
            />
            {
                props.items.map
                (item =>
                    <div className='product-container'
                         key={JSON.stringify(item.id)}>

                        <Button modification='add-to-chart'
                                onClick={openModal}
                                text='Add to cart'
                                toCart={JSON.stringify(item)}/>
                        <ProductItem item={item}
                                     onClick={props.onClick}
                                     showStar={true}
                                     isActive={favouritesArray.find((elem) => elem.id === item.id)}
                                     showCross={false}
                        />
                    </div>
                )
            }
        </div>
    );
}

ProductList.propTypes = {
    items: PropTypes.array,
    onClick: PropTypes.func
}