import React, {useState} from 'react';
import Modal from "../Modal";
import ProductItem from "../ProductItem";
import {useDispatch, useSelector} from "react-redux";
import {cartDeleteAction} from "../../redux/actions/cartAction";
import {modalHideAction, modalShowAction} from "../../redux/actions/modalAction";

export default function Cart() {

    const [product, setProduct] = useState({});
    const cartArray = useSelector(state => state.cartReducer.cart);
    const showModal = useSelector(state => state.modalReducer.show);
    const dispatch = useDispatch();

    const deleteItem = (e) => {
        let product = JSON.parse(e.target.dataset.cart);
        let selectedItem = cartArray.indexOf(product);
        cartArray.splice(selectedItem, 1);
        dispatch(cartDeleteAction(selectedItem));
        localStorage.setItem('cart', JSON.stringify(cartArray));
    }

    const openModal = (e) => {
        dispatch(modalShowAction())
        setProduct(e.target.dataset.product);
    }

    const closeModal = () => {
        dispatch(modalHideAction())
    }

    return (
        <div>
            <h3>This is Cart Page</h3>
            <h4>There are {cartArray.length} items in cart</h4>
            <div className='container'>
                <Modal show={showModal}
                       product={product}
                       onSuccessAdd={deleteItem}
                       onClick={closeModal}
                       onClose={closeModal}
                       textBody={'Delete?'}
                       textHeader={'Are you sure you want to delete this?'}
                />
                {
                    cartArray.map
                    (item =>
                        <div className='product-container'
                             key={JSON.stringify(item.id)}>

                            <ProductItem item={item} showArrow={false} showCross={true}
                                         onClick={openModal}/>
                        </div>
                    )
                }
            </div>
        </div>
    );
}