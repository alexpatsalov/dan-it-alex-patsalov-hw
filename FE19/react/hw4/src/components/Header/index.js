import React from 'react';
import './Header.scss';
import PropTypes from "prop-types";
import {useSelector} from "react-redux";

export default function Header() {

    const favouritesArray = useSelector(state => state.favouritesReducer.favourites);
    const cartArray = useSelector(state => state.cartReducer.cart);

    return (
        <div className='header-container'>
            <div className='header-chart'>{cartArray.length} items in cart</div>
            <div className='header-favourites'>{favouritesArray.length} items in favourites</div>
        </div>
    );

}

Header.propTypes = {
    cartArray: PropTypes.array,
    favouritesArray: PropTypes.array
}