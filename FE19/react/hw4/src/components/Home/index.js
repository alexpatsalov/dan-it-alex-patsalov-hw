import React from 'react';
import ProductList from "../ProductList";
import {useEffect, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {productListAction} from "../../redux/actions/productListAction";
import {favouritesAddAction} from "../../redux/actions/favouritesAction";
import {cartAddAction} from "../../redux/actions/cartAction";
import Header from "../Header";

export default function Home(){

    const [favourites, setFavourites] = useState([]);
    const [cart, setCart] = useState([]);
    const productList = useSelector(state => state.productsListReducer.products);
    const favouritesArrayRedux = useSelector(state => state.favouritesReducer.favourites);
    const cartArrayRedux = useSelector(state => state.cartReducer.cart);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(productListAction())
    }, [dispatch])

    const addToFavourites = (e) => {
        let clickedStar = e.target;
        let clickedProduct = JSON.parse(e.target.dataset.product);
        let index = favouritesArrayRedux.findIndex(item => item.id === clickedProduct.id);
        let clonedFavourites = [...favourites];
        if (index < 0) {
            clickedStar.classList.toggle('button-star-clicked');
            favouritesArrayRedux.push(clickedProduct);
            clonedFavourites.push(clickedProduct);
            dispatch(favouritesAddAction(favouritesArrayRedux));
            setFavourites([...clonedFavourites]);
            localStorage.setItem('favourites', JSON.stringify(favouritesArrayRedux));
        }
    }

    const addToCart = (e) => {
        let product = JSON.parse(e.target.dataset.cart);
        let clonedCart = [...cart];
        clonedCart.push(product);
        setCart([...clonedCart]);
        cartArrayRedux.push(product);
        dispatch(cartAddAction(cartArrayRedux));
        localStorage.setItem('cart', JSON.stringify(cartArrayRedux));
    }

    return(
        <div>
            <Header/>
            <ProductList items={productList}
                         onClick={addToFavourites}
                         onSuccess={addToCart}/>
        </div>
    );
}