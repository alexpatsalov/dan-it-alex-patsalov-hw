import React from 'react';
import ProductItem from "../ProductItem";
import {useDispatch, useSelector} from "react-redux";
import {favouritesDeleteAction} from "../../redux/actions/favouritesAction";

export default function Favourites() {

    const favouritesArray = useSelector(state => state.favouritesReducer.favourites);
    const dispatch = useDispatch();

    const deleteItem = (e) => {
        if (!favouritesArray.includes(e.target.dataset.product)) {
            const filtered = favouritesArray.filter((item) => item.id !== JSON.parse(e.target.dataset.product).id);
            dispatch(favouritesDeleteAction(filtered));
            localStorage.setItem('favourites', JSON.stringify(filtered));
        }
    }

    return (
        <div>
            <h3>This is Favourites Page</h3>
            <h4>There are {favouritesArray.length} items in favourites</h4>
            <div className='container'>
                {
                    favouritesArray.map
                    (item =>
                        <div className='product-container'
                             key={JSON.stringify(item.id)}>
                            <ProductItem item={item} showStar={true} showCross={false}
                                         onClick={deleteItem}/>
                        </div>
                    )
                }
            </div>
        </div>
    );
}