import {LOAD_PRODUCTS} from "../actions/productListAction";

const initialState = {
    products: []
}

export function productReducer(state = initialState, action){

    switch (action.type){

        case LOAD_PRODUCTS:
            return {
                ...state,
                products: action.payload
            }

        default:
            return state
    }
}