import {ADD_TO_FAVOURITES, DELETE_FROM_FAVOURITES} from "../actions/favouritesAction";

const initialState = {
    favourites: []
}

export function favouritesReducer(state = initialState, action){

    switch (action.type){

        case ADD_TO_FAVOURITES:
            console.log('add--->', state)
            return {
                ...state,
                favourites: action.payload
            }

        case DELETE_FROM_FAVOURITES:
            console.log('delete--->', state)
            return {
                favourites: action.payload
            }

        default:
            return state
    }
}