import {productReducer} from "./product";
import {favouritesReducer} from "./favourites";
import {cartReducer} from "./cart";
import {combineReducers} from "redux";
import {modalReducer} from "./modal";

export const rootReducer = combineReducers
({
    productsListReducer: productReducer,
    favouritesReducer: favouritesReducer,
    cartReducer: cartReducer,
    modalReducer: modalReducer
})