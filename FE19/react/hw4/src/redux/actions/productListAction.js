import getProductList from "../../services/productService";

export const LOAD_PRODUCTS = 'LOAD_PRODUCTS';

export const productListAction = () => async (dispatch) =>{

    const {data} = await getProductList();
    console.log(data);
    dispatch({
        type: LOAD_PRODUCTS,
        payload: data
    })
}