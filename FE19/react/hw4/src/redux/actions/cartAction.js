export const ADD_TO_CART = 'ADD_TO_CART';
export const DELETE_FROM_CART = 'DELETE_FROM_CART';

export const cartAddAction = (item) => (dispatch) => {

    dispatch({
        type: ADD_TO_CART,
        payload: item
    })
}

export const cartDeleteAction = (array) => (dispatch) => {

    dispatch({
        type: DELETE_FROM_CART,
        payload: array
    })
}