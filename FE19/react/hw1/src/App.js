import React from 'react';
import Container from "./components/Container";

export default class App extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <>
                <Container text='open first modal'
                           header='Do you want to delete this file?'
                           content='Once you delete this file, it will not be
                            possible to undo this action. Are you sure you want to delete it?'
                           isCloseButtonAvailable={true}
                           buttonActions={true}
                           style={{backgroundColor: "#ff00ff"}}
                />
                <Container text='open second modal'
                           header='second modal header'
                           content='second modal content'
                           isCloseButtonAvailable={false}
                           buttonActions={true}
                           style={{backgroundColor: "#000"}}
                />
            </>
        );
    }
}
