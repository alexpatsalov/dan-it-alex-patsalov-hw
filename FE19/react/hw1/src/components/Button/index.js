import React from 'react';
import PropTypes from 'prop-types';
import './Button.scss'

export default class Button extends React.Component {
    constructor(props) {
        super(props);

        this.onBtnClick = this.onBtnClick.bind(this);
    }

    onBtnClick() {
        this.props.onClick();
    }

    render() {

        return (
            <button className='button button-modal' onClick={this.onBtnClick} style={this.props.style}>{this.props.text}</button>
        );

    }
}

Button.propTypes = {
    onBtnClick: PropTypes.func,
    title: PropTypes.string
}