import React from 'react';
import PropTypes from 'prop-types';
import Button from "../Button";
import Modal from "../Modal";

export default class Container extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            show: false
        }
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    openModal() {
        this.setState({
            show: true
        })
    }

    closeModal() {
        this.setState({
            show: false
        })
    }

    render() {

        return (
            <>
                <div>
                    <Button onClick={this.openModal} text={this.props.text} style={this.props.style}/>
                    <Modal show={this.state.show}
                           header={this.props.header}
                           content={this.props.content}
                           isCloseButtonAvailable={this.props.isCloseButtonAvailable}
                           buttonActions={this.props.buttonActions}
                           onClose={this.closeModal}
                    />
                </div>
            </>
        );
    }
}

Container.propTypes = {
    openModal: PropTypes.func,
    closeModal: PropTypes.func
}
