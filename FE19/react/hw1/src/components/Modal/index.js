import React from 'react';
import PropTypes from 'prop-types';
import Button from "../Button";
import './Modal.scss';


export default class Modal extends React.Component {
    constructor(props) {
        super(props);
        this.onClose = this.onClose.bind(this);
        this.onCloseArea = this.onCloseArea.bind(this);
    }

    onClose() {
        this.props.onClose()
    }
    onCloseArea(e){
        if(e.target.className === 'modal'){
            this.onClose()
        }
    }

    render() {
        let displayModal = {
            display: this.props.show ? 'flex' : 'none'
        }
        let className = 'modal-content';
        if(!this.props.isCloseButtonAvailable){
            className+=' modal-content-alternative'
        }
        let buttonActionStyle = {
            width: '100px',
            backgroundColor: 'rgba(0, 0, 0, 0.2)',
            padding: '7px'
        };

        return (
            <div className='modal' style={displayModal} onClick={this.onCloseArea}>
                <div className={className}>
                    <div className='modal-header'>
                        <h4>
                            {this.props.header}
                        </h4>
                        {this.props.isCloseButtonAvailable
                            ? (
                                <div className='button-container'>
                                    <Button onClick={this.onClose} text='&times;'/>
                                </div>
                            )
                            : null}
                    </div>
                    <div className='modal-body'>
                        <p>{this.props.content}</p>
                    </div>
                    <div className='modal-footer'>
                        {this.props.buttonActions
                            ? (<>
                                <Button
                                    style={buttonActionStyle}
                                    onClick={this.onClose} text='Ok'/>
                                <Button
                                    style={buttonActionStyle}
                                    onClick={this.onClose} text='Cancel'/>
                            </>)
                            : null}
                    </div>
                </div>
            </div>
        );
    }
}

Modal.propTypes = {
    header: PropTypes.string,
    isCloseButtonAvailable: PropTypes.bool,
    content: PropTypes.string,
    buttonActions: PropTypes.bool,
    onBtnCLick: PropTypes.func
}
