document.addEventListener("DOMContentLoaded", ready);
function ready() {
	let button = document.getElementById('button');
	let icon = document.getElementById('icon');
	let navMenuList = document.getElementById('navMenuList')
	let navMenu = document.getElementById('navMenu');
	button.addEventListener('click', function () {
		icon.classList.toggle('fa-bars');
		icon.classList.toggle('fa-times');
		document.getElementById('navMenu').classList.toggle('nav-menu-active');
		navMenuList.classList.toggle('nav-menu-list-active');
		navMenuList.classList.toggle('nav-menu-list');
		let menuItems = document.getElementsByClassName('nav-menu-item');
		for (let el of menuItems){
			el.classList.toggle('nav-menu-item-active');
			el.classList.toggle('nav-menu-item-active:hover');
		}
	});
}
function click() {

}
